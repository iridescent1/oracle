### 实验一

班级：软件工程20-2  

学号：202010414228

姓名：周波

#### 1.实验名称

SQL语句的执行计划分析与优化指导

#### 2.实验目的

分析SQL执行计划，执行SQL语句的优化指导。理解分析SQL语句的执行计划的重要作用。

#### 3.实验内容

- 对Oracle12c中的HR人力资源管理系统中的表进行查询与分析。
- 设计自己的查询语句，并作相应的分析，查询语句不能太简单。执行两个比较复杂的返回相同查询结果数据集的SQL语句，通过分析SQL语句各自的执行计划，判断哪个SQL语句是最优的。最后将你认为最优的SQL语句通过sqldeveloper的优化指导工具进行优化指导，看看该工具有没有给出优化建议。

#### 4.实验步骤

1. 执行查询语句1：

   ```sql
   SELECT d.department_name,count(e.job_id)as "部门总人数",
   avg(e.salary)as "平均工资"
   from hr.departments d,hr.employees e
   where d.department_id = e.department_id
   and d.department_name in ('IT','Sales')
   GROUP BY d.department_name;
   ```

2. 查看结果

![](pict1.png)

![](pict2.png)

3. 执行查询语句2：

   ```sql
   SELECT d.department_name,count(e.job_id)as "部门总人数",
   avg(e.salary)as "平均工资"
   FROM hr.departments d,hr.employees e
   WHERE d.department_id = e.department_id
   GROUP BY d.department_name
   HAVING d.department_name in ('IT','Sales');
   ```

4. 查看结果

   ![](pict3.png)

   ![](pict4.png)

5. 运行优化指导：

   1. 运行`grant advisor to hr`为hr用户添加advisor权限

   2. 在sqldeveloper中新建查询点击优化指导

      1. 对于查询1，存在优化建议：

      ![](pict5.png)

      1. 对于查询2，无优化建议

      ![](pict6.png)