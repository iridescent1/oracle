DECLARE
  TYPE t_product_type IS TABLE OF product%ROWTYPE;
  products t_product_type;
BEGIN
  products := t_product_type();
  FOR i IN 1..50000 LOOP
    products.EXTEND; 
    products(i).product_id := i;
    products(i).name := 'Product ' || i;
    products(i).description := 'This is product ' || i;
    products(i).type := 'Type ' || MOD(i, 10);
    products(i).price := ROUND(DBMS_RANDOM.VALUE(10, 1000), 2);
    products(i).stock := ROUND(DBMS_RANDOM.VALUE(0, 100), 0);
  END LOOP;
  FORALL i IN 1..products.COUNT
    INSERT INTO product VALUES products(i);
  COMMIT


  DECLARE
  TYPE t_order_type IS TABLE OF "order"%ROWTYPE;
  orders t_order_type;
BEGIN
  orders := t_order_type();
  FOR i IN 1..50000 LOOP
    orders.EXTEND; 
    orders(i).order_id := i;
    orders(i).user_id := ROUND(DBMS_RANDOM.VALUE(1, 10), 0);
    orders(i).order_time := SYSDATE - ROUND(DBMS_RANDOM.VALUE(0, 30), 0);
    orders(i).status := CASE MOD(i, 3) 
                           WHEN 0 THEN 'NEW' 
                           WHEN 1 THEN 'PROCESSING' 
                           ELSE 'COMPLETED' 
                         END;
    orders(i).amount := ROUND(DBMS_RANDOM.VALUE(10, 500), 2);
  END LOOP;
  FORALL i IN 1..orders.COUNT
    INSERT INTO "order" VALUES orders(i);
  COMMIT

DECLARE
  TYPE t_user_type IS TABLE OF "user"%ROWTYPE;
  users t_user_type;
BEGIN
  users := t_user_type();
  users.EXTEND;
  users(1).user_id := 1;
  users(1).username := 'admin';
  users(1).password := 'admin_password';
  users(1).contact := 'admin@example.com';
  users(1).address := '123 Main St, Anytown USA';
  
  FOR i IN 2..5000 LOOP
    users.EXTEND;
    users(i).user_id := i;
    users(i).username := 'user' || i;
    users(i).password := 'password' || i;
    users(i).contact := 'user' || i || '@example.com';
    users(i).address := '456 Main St, Anytown USA';
  END LOOP;
  FORALL i IN 1..users.COUNT
    INSERT INTO "user" VALUES users(i);
  COMMIT;
END;

DECLARE
  TYPE t_payment_type IS TABLE OF payment%ROWTYPE;
  payments t_payment_type;
BEGIN
  payments := t_payment_type();
  FOR i IN 1..50000 LOOP
    payments.EXTEND;
    payments(i).payment_id := i;
    payments(i).order_id := ROUND(DBMS_RANDOM.VALUE(1, 100), 0);
    payments(i).payment_time := SYSDATE - ROUND(DBMS_RANDOM.VALUE(0, 30), 0);
    payments(i).amount := ROUND(DBMS_RANDOM.VALUE(10, 500), 2);
  END LOOP;
  FORALL i IN 1..payments.COUNT
    INSERT INTO payment VALUES payments(i);
  COMMIT;
END;
