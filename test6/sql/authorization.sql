-- 1. 创建管理员用户和普通用户
CREATE USER admin IDENTIFIED BY admin_password;
CREATE USER normal_user IDENTIFIED BY user_password;

-- 2. 创建管理员角色和普通用户角色
CREATE ROLE admin_role;
CREATE ROLE user_role;

-- 3. 为管理员角色授权所有表的所有权限，为普通用户角色授权商品表的查询权限
GRANT ALL PRIVILEGES ON product TO admin_role;
GRANT ALL PRIVILEGES ON "order" TO admin_role;
GRANT ALL PRIVILEGES ON "user" TO admin_role;
GRANT ALL PRIVILEGES ON payment TO admin_role;

GRANT SELECT ON product TO user_role;

-- 4. 将管理员用户分配到管理员角色中，将普通用户分配到普通用户角色中
GRANT admin_role TO admin;
GRANT user_role TO normal_user;
 
-- 5. 给管理员角色和普通用户角色分别赋予对应的权限
GRANT admin_role TO admin;
GRANT user_role TO normal_user;
