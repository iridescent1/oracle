--------------------------------------------------------
--  文件已创建 - 星期三-五月-24-2023   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Table PRODUCT
--------------------------------------------------------

  CREATE TABLE "SYSTEM"."PRODUCT" 
   (	"PRODUCT_ID" NUMBER(10,0), 
	"NAME" VARCHAR2(100 BYTE), 
	"DESCRIPTION" VARCHAR2(4000 BYTE), 
	"TYPE" VARCHAR2(50 BYTE), 
	"PRICE" NUMBER(10,2), 
	"STOCK" NUMBER(10,0)
   ) PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 
 ROW STORE COMPRESS ADVANCED LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "DATA_TBS" ;
  GRANT SELECT ON "SYSTEM"."PRODUCT" TO "USER_ROLE";
  GRANT ALTER ON "SYSTEM"."PRODUCT" TO "ADMIN_ROLE";
  GRANT DELETE ON "SYSTEM"."PRODUCT" TO "ADMIN_ROLE";
  GRANT INSERT ON "SYSTEM"."PRODUCT" TO "ADMIN_ROLE";
  GRANT SELECT ON "SYSTEM"."PRODUCT" TO "ADMIN_ROLE";
  GRANT UPDATE ON "SYSTEM"."PRODUCT" TO "ADMIN_ROLE";
  GRANT READ ON "SYSTEM"."PRODUCT" TO "ADMIN_ROLE";
  GRANT ON COMMIT REFRESH ON "SYSTEM"."PRODUCT" TO "ADMIN_ROLE";
  GRANT QUERY REWRITE ON "SYSTEM"."PRODUCT" TO "ADMIN_ROLE";
  GRANT DEBUG ON "SYSTEM"."PRODUCT" TO "ADMIN_ROLE";
  GRANT FLASHBACK ON "SYSTEM"."PRODUCT" TO "ADMIN_ROLE";
--------------------------------------------------------
--  DDL for Index SYS_C007414
--------------------------------------------------------

  CREATE UNIQUE INDEX "SYSTEM"."SYS_C007414" ON "SYSTEM"."PRODUCT" ("PRODUCT_ID") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "DATA_TBS" ;
--------------------------------------------------------
--  Constraints for Table PRODUCT
--------------------------------------------------------

  ALTER TABLE "SYSTEM"."PRODUCT" ADD PRIMARY KEY ("PRODUCT_ID")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "DATA_TBS"  ENABLE;
