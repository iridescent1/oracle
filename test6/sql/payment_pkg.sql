-- 创建 order_details表
CREATE TABLE order_details (
  order_id NUMBER(10) NOT NULL,
  product_id NUMBER(10) NOT NULL,
  quantity NUMBER(10) NOT NULL,
  price NUMBER(10, 2) NOT NULL,
  CONSTRAINT order_details_pk PRIMARY KEY (order_id, product_id),
  CONSTRAINT order_details_order_fk FOREIGN KEY (order_id) REFERENCES "order" (order_id),
  CONSTRAINT order_details_product_fk FOREIGN KEY (product_id) REFERENCES product (product_id)
);


CREATE OR REPLACE PACKAGE payment_pkg AS
  -- 查询某个订单的支付情况
  FUNCTION get_payment_status(order_id IN NUMBER) RETURN VARCHAR2;
  -- 查询某个用户的所有订单
  PROCEDURE get_orders_for_user(user_id IN NUMBER, orders OUT SYS_REFCURSOR);
  -- 查询某个用户的所有支付记录
  PROCEDURE get_payments_for_user(user_id IN NUMBER, payments OUT SYS_REFCURSOR);
  -- 创建一个新的支付记录，并将其保存到数据库中
  PROCEDURE create_payment(order_id IN NUMBER, amount IN NUMBER);
  -- 取消某个支付记录
  PROCEDURE cancel_payment(payment_id IN NUMBER);
  -- 查询所有已支付的订单中的商品销售情况
  PROCEDURE get_sales_report(report OUT SYS_REFCURSOR);
END payment_pkg;
/

CREATE OR REPLACE PACKAGE BODY payment_pkg AS
  -- 查询某个订单的支付情况
  FUNCTION get_payment_status(order_id IN NUMBER) RETURN VARCHAR2 AS
    payment_status VARCHAR2(50);
  BEGIN
    SELECT status INTO payment_status FROM "order" WHERE order_id = order_id;
    RETURN payment_status;
  END get_payment_status;

  -- 查询某个用户的所有订单
  PROCEDURE get_orders_for_user(user_id IN NUMBER, orders OUT SYS_REFCURSOR) AS
  BEGIN
    OPEN orders FOR
      SELECT o.order_id, o.order_time, o.amount, o.status, p.product_id, p.name, p.price, od.quantity
      FROM "order" o
      JOIN order_details od ON o.order_id = od.order_id
      JOIN product p ON od.product_id = p.product_id
      WHERE o.user_id = user_id;
  END get_orders_for_user;

  -- 查询某个用户的所有支付记录
 PROCEDURE get_payments_for_user(user_id IN NUMBER, payments OUT SYS_REFCURSOR) AS
  BEGIN
    OPEN payments FOR
      SELECT payment_id, order_id, payment_time, amount
      FROM payment
      WHERE order_id IN (SELECT order_id FROM "order" WHERE user_id = user_id);
  END get_payments_for_user;

  -- 创建一个新的支付记录，并将其保存到数据库中
  PROCEDURE create_payment(order_id IN NUMBER, amount IN NUMBER) AS
    payment_id NUMBER(10);
  BEGIN
    -- 生成新的支付记录ID
    SELECT payment_seq.NEXTVAL INTO payment_id FROM DUAL;

    -- 插入新的支付记录
    INSERT INTO payment (payment_id, order_id, payment_time, amount)
    VALUES (payment_id, order_id, SYSDATE, amount);

    -- 更新订单状态为"PAID"
    UPDATE "order" SET status = 'PAID' WHERE order_id = order_id;

    -- 提交事务
    COMMIT;
  END create_payment;

  -- 取消某个支付记录
  PROCEDURE cancel_payment(payment_id IN NUMBER) AS
    order_id NUMBER(10);
  BEGIN
    -- 查询支付记录对应的订单ID
    SELECT order_id INTO order_id FROM payment WHERE payment_id = payment_id;

    -- 删除支付记录
    DELETE FROM payment WHERE payment_id = payment_id;

    -- 更新订单状态为"UNPAID"
    UPDATE "order" SET status = 'UNPAID' WHERE order_id = order_id;

    -- 提交事务
    COMMIT;
  END cancel_payment;

  -- 查询所有已支付的订单中的商品销售情况
  PROCEDURE get_sales_report(report OUT SYS_REFCURSOR) AS
  BEGIN
    OPEN report FOR
      SELECT p.product_id, p.name, SUM(od.quantity) AS total_quantity, SUM(od.quantity * p.price) AS total_sales
      FROM "order" o
      JOIN order_details od ON o.order_id = od.order_id
      JOIN product p ON od.product_id = p.product_id 
      WHERE o.status = 'PAID'
      GROUP BY p.product_id, p.name;
  END get_sales_report;
END payment_pkg;
/
