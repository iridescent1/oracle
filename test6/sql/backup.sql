-- 创建备份目录
CREATE DIRECTORY backup_dir AS '/u01/backup';

-- 创建备份用户
CREATE USER backup_user IDENTIFIED BY password;

-- 授予备份用户备份权限
GRANT RECOVERY_CATALOG_OWNER TO backup_user;

-- 创建RMAN备份脚本
CREATE OR REPLACE PROCEDURE backup_database AS
BEGIN
-- 执行完全备份
DBMS_BACKUP_RESTORE.BACKUP_DATABASE(
backup_type => DBMS_BACKUP_RESTORE.FULL_BACKUP,
backup_destination => '/u01/backup/full_backup_%d_%T.bkp',
backup_format => 'SBT_TAPE',
device_params => 'ENV=(BACKUP_DIR=/u01/backup)',
compression => DBMS_BACKUP_RESTORE.COMPRESS_FOR_SPEED
);

-- 执行增量备份
DBMS_BACKUP_RESTORE.BACKUP_DATABASE(
backup_type => DBMS_BACKUP_RESTORE.INCREMENTAL_BACKUP,
backup_destination => '/u01/backup/incr_backup_%d_%T.bkp',
backup_format => 'SBT_TAPE',
device_params => 'ENV=(BACKUP_DIR=/u01/backup)',
compression => DBMS_BACKUP_RESTORE.COMPRESS_FOR_SPEED
);
END backup_database;
/

-- 创建定期备份计划
BEGIN
DBMS_SCHEDULER.CREATE_JOB (
job_name => 'daily_backup',
job_type => 'STORED_PROCEDURE',
job_action => 'backup_database',
start_date => SYSDATE,
repeat_interval => 'FREQ=DAILY',
enabled => TRUE
);
END;
/

-- 创建实时备份计划
BEGIN
DBMS_SCHEDULER.CREATE_JOB (
job_name => 'hourly_backup',
job_type => 'STORED_PROCEDURE',
job_action => 'backup_database',
start_date => SYSDATE,
repeat_interval => 'FREQ=HOURLY',
enabled => TRUE
);
END;
/
